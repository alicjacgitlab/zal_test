from datetime import datetime

from django.db import models


class Todo(models.Model):
    text = models.CharField(max_length=70)
    complete = models.BooleanField(default=False)
    add_data = models.DateTimeField(default=datetime.now, editable=True)

    def __str__(self):
        return self.text

