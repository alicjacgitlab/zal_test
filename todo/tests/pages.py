from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support.ui import Select


class Page:
    BASE_URL = "http://localhost:8000/"

    def __init__(self, driver=None):
        if driver is None:
            self.driver = webdriver.Chrome()
            self.driver.get(self.BASE_URL)
        else:
            self.driver = driver


class TaskPage(Page):
    def __init__(self, driver=None):
        super(TaskPage, self).__init__(driver)

    def goto(self):
        self.driver.get(self.BASE_URL)

    def add_task(self, task_name):
        self.driver.find_element_by_name("text").send_keys(task_name)
        self.driver.find_element_by_id("add-btn").click()

    def get_tasks(self):
        return self.driver.find_elements_by_xpath("//li")

    def click_delete_all_tasks(self):
        self.driver.find_elements_by_class_name("btn-warning")[1].click()

    def click_delete_all_done_tasks(self):
        self.driver.find_elements_by_class_name("btn-warning")[0].click()

    def mark_first_task_as_completed(self):
        self.get_tasks()[-1].click()

    def is_fist_task_completed(self):
        return "todo-completed" in self.get_tasks()[-1].get_attribute("class")


class AdminPage(Page):
    LOGING = "admin"
    PASSWORD = "admin"

    def __init__(self, driver=None):
        super(AdminPage, self).__init__(driver)

    def goto(self):
        self.driver.get(self.BASE_URL + "admin")

    def login(self):
        self.goto()
        try:
            self.driver.find_element_by_name("username").send_keys(self.LOGING)
            self.driver.find_element_by_name("password").send_keys(self.LOGING)
            self.driver.find_element_by_xpath("//*[@id=\"login-form\"]/div[3]/input").click()
        except NoSuchElementException:
            pass

    def goto_todos(self):
        self.driver.find_element_by_xpath("//*[@id=\"content-main\"]/div[2]/table/tbody/tr/th/a").click()

    def add_task(self, task_name, completed=False):
        self.goto()
        self.goto_todos()
        self.driver.find_element_by_xpath("//*[@id=\"content-main\"]/ul/li/a").click()
        self.driver.find_element_by_xpath("//*[@id=\"id_text\"]").send_keys(task_name)
        if completed is True:
            self.driver.find_element_by_name("complete").click()
        self.driver.find_element_by_xpath("//*[@id=\"todo_form\"]/div/div/input[1]").click()

    def delete_all_tasks(self):
        self.goto()
        self.goto_todos()
        try:
            chb_select_all = self.driver.find_element_by_xpath("//*[@id=\"action-toggle\"]")
        except NoSuchElementException:
            return
        chb_select_all.click()
        select = Select(self.driver.find_element_by_xpath("//*[@id=\"changelist-form\"]/div[1]/label/select"))
        select.select_by_index(1)
        btn_ok = self.driver.find_element_by_xpath("//*[@id=\"changelist-form\"]/div[1]/button")
        btn_ok.click()
        btn_confirmation = self.driver.find_element_by_xpath("//*[@id=\"content\"]/form/div/input[last()]")
        btn_confirmation.click()
